package com.devcamp.countryregionapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.countryregionapi.model.Country;
import com.devcamp.countryregionapi.service.CountryService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CountryController {
    @Autowired
    private CountryService countryService;

    @GetMapping("/countries")
    public ArrayList<Country> countries() {
        ArrayList<Country> countriesList = countryService.countriesList();
        return countriesList;
    }

    @GetMapping("/country-info")
    public Country countriesInfo(@RequestParam(required = true, name = "code") String countryCode) {
        ArrayList<Country> countriesList = countryService.countriesList();
        Country findCountry = new Country();
        for (Country country : countriesList) {
            if (country.getCountryCode().equals(countryCode)) {
                findCountry = country;
            }

        }
        return findCountry;
    }

    @GetMapping("/countries/{index}")
    public Country country(@PathVariable int index) {
        Country country = countryService.findCountryIndex(index);
        return country;
    }

}
