package com.devcamp.countryregionapi.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.countryregionapi.model.Country;

@Service
public class CountryService {
    @Autowired
    private RegionService regionService;
    Country vietNam = new Country("VN", "VietNam");
    Country USA = new Country("USA", "USA");
    Country AUS = new Country("AUS", "Australia");
    Country CAN = new Country("CAN", "Canada");
    Country JP = new Country("JP", "Japan");
    Country CHINA = new Country("CHINA", "China");

    public ArrayList<Country> countriesList() {
        vietNam.setRegions(regionService.regionsVietNam());
        USA.setRegions(regionService.regionsUSA());
        AUS.setRegions(regionService.regionsUs());
        CAN.setRegions(regionService.regionsCanada());
        JP.setRegions(regionService.regionsJapan());
        CHINA.setRegions(regionService.regionsTQ());

        ArrayList<Country> countries = new ArrayList<>();
        countries.add(vietNam);
        countries.add(USA);
        countries.add(AUS);
        countries.add(CAN);
        countries.add(CHINA);
        countries.add(JP);
        return countries;

    }

    public Country findCountryIndex(int index) {
        ArrayList<Country> countries = countriesList();
        Country country = new Country();
        if (index < 0 || index > 5) {
            return country;
        } else {
            country = countries.get(index);
        }
        return country;

    }

}
